/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/


/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment constiables from your Lambda function
const environment = process.env.ENV
const region = process.env.REGION
const authFaceitsiteb66f03c8b66f03c8UserPoolId = process.env.AUTH_FACEITSITEB66F03C8B66F03C8_USERPOOLID

Amplify Params - DO NOT EDIT */

const express = require('express');
const bodyParser = require('body-parser');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');

const DDB = require('aws-sdk/clients/dynamodb');

const ddb = new DDB();

const TableName = 'FaceItLog';

// declare a new express app
const app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function (req, res, next) {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Headers', 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token');
    next();
});


/**********************
 * Example get method *
 **********************/

app.get('/', function (req, res) {
    // Add your code here
    res.json({success: 'get call succeed!', url: req.url});
});

app.get('/list', async (req, res) => {
    const Limit = req.param('limit') ? req.param('limit') : 11;
    const params = {
        TableName,
        Select: 'ALL_ATTRIBUTES',
        Limit,
        ExpressionAttributeValues: {
            ":game": {
                S: req.param('game')
            },
            ":targetStatus": {
                S: 'complete'
            }
        },
        KeyConditionExpression: "game = :game",
        ScanIndexForward: false,
        ExpressionAttributeNames: {
            "#status": 'status'
        },
        FilterExpression: "#status = :targetStatus"
    };
    try {
        const result = await ddb.query(params).promise();
        res.json(result);
    } catch (e) {
        res.status(400);
        res.json(e);
    }
});

app.get('/*', function (req, res) {
    // Add your code here
    res.json({success: 'get call succeed!', url: req.url});
});

/****************************
 * Example post method *
 ****************************/

app.post('/', function (req, res) {
    // Add your code here
    res.json({success: 'post call succeed!', url: req.url, body: req.body});
});

app.post('/*', function (req, res) {
    // Add your code here
    res.json({success: 'post call succeed!', url: req.url, body: req.body});
});

/****************************
 * Example put method *
 ****************************/

app.put('/', function (req, res) {
    // Add your code here
    res.json({success: 'put call succeed!', url: req.url, body: req.body});
});

app.put('/*', function (req, res) {
    // Add your code here
    res.json({success: 'put call succeed!', url: req.url, body: req.body});
});

/****************************
 * Example delete method *
 ****************************/

app.delete('/', function (req, res) {
    // Add your code here
    res.json({success: 'delete call succeed!', url: req.url});
});

app.delete('/*', function (req, res) {
    // Add your code here
    res.json({success: 'delete call succeed!', url: req.url});
});

app.listen(3000, function () {
    console.log("App started");
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app;
