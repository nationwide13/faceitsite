import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import ReactBootstrapSlider from 'react-bootstrap-slider';

import { GRAPH_COLORS } from './utilities/constants';
import { API, Analytics } from './utilities/amplify';
import Header from './components/header';
import Graph from './components/graph';

export default class extends React.Component {
    eventSubmitted = false;
    ELO_KEY = 'elo';
    KD_KEY = 'kd';

    constructor(props) {
        super(props);
        this.state = {
            matches: [],
            loadError: undefined,
            players: {},
            minElo: 9999999999999,
            maxElo: 0,
            loading: true,
            averageKd: 0,
            currentKey: this.ELO_KEY,
            optionsExpanded: false,
            matchesShown: [0, 100]
        };
    }

    componentWillReceiveProps(newProps) {
        if (newProps.authData && !this.eventSubmitted) {
            this.eventSubmitted = true;
            Analytics.record({name: 'homepage', attributes: {username: newProps.authData.username}});
        }
    }

    componentDidMount() {
        this.loadMatches();
    }

    render() {
        if (this.props.authState !== 'signedIn' || this.state.loading) {
            return null;
        }
        const username = this.props.authData.username;
        return (
            <div>
                <Header username={username}>
                    <Nav>
                        <Button
                            variant="secondary"
                            onClick={() => this.setState({currentKey: this.state.currentKey === this.ELO_KEY ? this.KD_KEY : this.ELO_KEY})}
                            style={{marginRight: '1rem'}}
                        >
                            {this.state.currentKey === this.ELO_KEY ? 'Elo' : `K/D (avg: ${this.state.averageKd})`}
                        </Button>
                        <DropdownButton id="dropdown-basic-button" title="Show players" variant="secondary" style={{marginRight: '1rem'}}>
                            {Object.keys(this.state.players).map((name, index) => (
                                <Dropdown.Item
                                    key={name + '-show'}
                                    onClick={() => {
                                        const players = this.state.players;
                                        players[name].show = !players[name].show;
                                        this.setState({players});
                                    }}
                                >
                                    <Form.Check
                                        type="checkbox"
                                        id={'check-' + name}
                                        label={name}
                                        checked={this.state.players[name].show}
                                        onChange={(event) => {
                                            const players = this.state.players;
                                            players[name].show = event.target.checked;
                                            this.setState({players});
                                        }}
                                    />
                                </Dropdown.Item>
                            ))}
                        </DropdownButton>
                        <DropdownButton title="Matches to show" variant="secondary">
                            <div className="drowdown-slider">
                                <ReactBootstrapSlider
                                    value={this.state.matchesShown}
                                    change={this.changeSliderPosition}
                                    // slideStop={({target}) => this.setState({matchesShown: target.value})}
                                    step={1}
                                    max={this.state.matches.length}
                                    min={1}
                                    disabled={this.state.loading ? 'disabled' : undefined}
                                />
                                <div style={{float: 'left', paddingLeft: '1rem'}}>{this.state.matchesShown[0]}</div>
                                <div style={{float: 'right', paddingRight: '1rem'}}>{this.state.matchesShown[1]}</div>
                            </div>
                        </DropdownButton>
                    </Nav>
                </Header>
                <div style={{marginTop: '1rem'}}>
                    <div>
                        {Object.keys(this.state.players).filter((name) => this.state.players[name].show).map((name) => (
                            <span
                                key={name + '-legend'}
                                style={{
                                    display: 'inline-block',
                                    margin: '0 1rem 1rem',
                                    color: this.state.players[name].color
                                }}
                            >
                                {name + ' - ' + (this.state.currentKey === this.ELO_KEY ? this.state.players[name].currentElo : this.state.players[name].averageKd)}
                            </span>
                        ))}
                    </div>
                </div>
                <Graph
                    currentKey={this.state.currentKey}
                    matches={this.state.matches}
                    players={this.state.players}
                    averageKd={this.state.averageKd}
                    maxElo={this.state.maxElo}
                    minElo={this.state.minElo}
                    loading={this.state.loading}
                    matchesShown={this.state.matchesShown}
                />
            </div>
        );
    }

    changeSliderPosition = ({target}) => {
        const matchesShown = target.value;
        const averageKd = this.calcAverageKd(matchesShown, this.state.players);
        const players = this.calcAveragePlayerKd(matchesShown, this.state.players);
        this.setState({matchesShown, averageKd, players});
    }

    calcAveragePlayerKd = (matchesShown, players) => {
        Object.keys(players).forEach((name) => {
            let samples = 0;
            const kd = players[name].kd
                .filter(({x}) => x >= matchesShown[0] - 1 && x < matchesShown[1])
                .reduce((total2, {y}) => {
                    samples++;
                    return total2 + parseFloat(y)
                }, 0);
            players[name].averageKd = Math.round(kd / samples * 100) / 100;
        });
        return players;
    }

    calcAverageKd = (matchesShown, players) => {
        let samples = 0;
        const kd = Object.keys(players)
            .filter((name) => players[name].show)
            .reduce((total, name) => total + players[name].kd
                .filter(({x}) => x >= matchesShown[0] - 1 && x < matchesShown[1])
                .reduce((total2, {y}) => {
                    samples++;
                    return total2 + parseFloat(y)
                }, 0), 0);
        return Math.round(kd / samples * 100) / 100;
    }

    loadMatches = async () => {
        try {
            const players = {};

            const result = await API.get('/list?game=battalion&limit=100');

            // const LastEvaluatedKey = result.LastEvaluatedKey;
            // ExclusiveStartKey
            const matches = result.Items.reverse();

            let minElo = this.state.minElo;
            let maxElo = this.state.maxElo;
            const kds = [];
            matches.forEach((match, index) => {
                const playerData = JSON.parse(match.playerData.S);
                playerData.forEach((player, idx) => {
                    const name = player.name;
                    const elo = player.afterElo;
                    const kd = player.kd;
                    if (!players[name]) {
                        players[name] = {
                            elo: [],
                            kd: [],
                            show: true
                        };
                    }
                    if (elo) {
                        if (elo > maxElo) {
                            maxElo = elo;
                        }
                        if (elo < minElo) {
                            minElo = elo;
                        }
                        players[name].elo.push({x: index, y: elo});
                        if (kd) {
                            players[name].kd.push({x: index, y: kd});
                            kds.push(parseFloat(kd));
                        }
                        players[name].currentElo = elo;
                    }

                });
            });
            Object.keys(players).forEach((player, index) => players[player].color = GRAPH_COLORS[index]);
            const matchesShown = [(matches.length - 45 > 0) ? matches.length - 45 : 1, matches.length];
            this.setState({matches, players: this.calcAveragePlayerKd(matchesShown, players), averageKd: this.calcAverageKd(matchesShown, players), maxElo, minElo, matchesShown, loading: false});
        } catch (loadError) {
            this.setState({loadError});
        }
    }
}
