import SignIn from './signIn';
import SignUp from './signUp';

export default {
    SignIn,
    SignUp
}
