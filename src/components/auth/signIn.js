import * as React from 'react';
import { Button, Form } from 'react-bootstrap';
import Auth from '@aws-amplify/auth';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            usernameError: undefined,
            passwordError: undefined,
            loginError: undefined,
            loading: false
        }
    }
    render() {
        return this.props.authState !== 'signIn' ? null : (
            <Form style={{maxWidth: '500px', margin: '75px auto', textAlign: 'left'}}>
                <h2>Login</h2>
                <Form.Group controlId="username">
                    <Form.Label>Enter username</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Username"
                        value={this.state.username}
                        onChange={(event) => this.setState({username: event.target.value})}
                        isInvalid={!!this.state.usernameError}
                    />
                    <Form.Control.Feedback type="invalid">{this.state.usernameError}</Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="password">
                    <Form.Label>Enter password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="password"
                        value={this.state.password}
                        onChange={(event) => this.setState({password: event.target.value})}
                        isInvalid={!!this.state.passwordError}
                    />
                    <Form.Control.Feedback type="invalid">{this.state.passwordError}</Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="formBasicEmail">
                    <Form.Control type="hidden" isInvalid={!!this.state.loginError}/>
                    <Button onClick={this.handleLogin} disabled={this.state.loading} style={{marginRight: '1rem'}}>Login</Button>
                    <Button onClick={() => this.props.onStateChange('signUp')} disabled={this.state.loading} variant="secondary">Sign up</Button>
                    <Form.Control.Feedback type="invalid">{this.state.loginError}</Form.Control.Feedback>
                </Form.Group>
            </Form>
        )
    }
    handleLogin = async () => {
        this.setState({loading: true});
        const state = this.state;
        state.loading = false;
        state.usernameError = undefined;
        state.passwordError = undefined;

        if (!state.username) {
            state.usernameError = 'Username cannot be blank';
        }
        if (!state.password) {
            state.passwordError = 'Password cannot be blank';
        }
        if (!state.usernameError && !state.passwordError) {
            try {
                const user = await Auth.signIn(state.username, state.password);
                this.props.onStateChange('signedIn', user);
            } catch (e) {
                state.loginError = e.message ? e.message : 'There was an error during login';
            }
        }
        this.setState(state);
    }
}
