import React from 'react';
import { XYPlot, XAxis, YAxis, HorizontalGridLines, LineMarkSeries as Line, Crosshair } from 'react-vis';

export default class extends React.Component {
    listener = undefined;
    PLOT_CLASSNAME = 'rv-xy-plot__inner';
    GRID_LINE_CLASSNAME = 'rv-xy-plot__grid-lines';
    Y_TICK_SIZE = 25;
    ELO_KEY = 'elo';
    KD_KEY = 'kd';
    constructor(props) {
        super(props);
        this.state = {
            crosshairValues: [],
            pageWidth: this.getPageWidth(),
        };
        this.listener = window.addEventListener('resize', () => this.setState({pageWidth: this.getPageWidth()}))
    }
    componentWillUnmount() {
        if (this.listener) {
            window.removeEventListener('resize', this.listener);
        }
    }
    render() {
        if (this.props.loading) {
            return null;
        }
        const eloYTicks = this.getEloYTicks();
        return (
            <div style={{alignItems: 'center', margin: '0 auto', padding: '0 20px'}}>
                <div style={{flex: 2}}>
                    <XYPlot
                        width={this.state.pageWidth - 40}
                        height={500}
                        margin={{bottom: 150}}
                        onMouseLeave={() => this.setState({crosshairValues: []})}
                        onMouseMove={({target, clientX}) => {
                            let checkCount = 0;
                            while (target.className.baseVal !== this.PLOT_CLASSNAME || checkCount > 5) {
                                target = target.parentElement;
                            }
                            target = target.getElementsByClassName(this.GRID_LINE_CLASSNAME)[0];
                            const rect = target.getBoundingClientRect();
                            const cursorX = clientX - rect.left;
                            const percent = cursorX / rect.width;
                            const xCoord = Math.round(Math.round(percent * 100000) / 100000 * (this.props.matchesShown[1] - this.props.matchesShown[0] + 1));
                            let crosshairValues = [];
                            if ((xCoord && isFinite(xCoord)) || xCoord === 0) {
                                const xVal = xCoord + this.props.matchesShown[0] - 2;
                                crosshairValues = Object.keys(this.props.players)
                                    .filter((name) => this.props.players[name].show)
                                    .map((name) => this.props.players[name][this.props.currentKey].find(({x}) => x === xVal));
                            }
                            this.setState({crosshairValues});
                        }}
                    >
                        <Line
                            opacity={0}
                            data={this.props.matches
                                .map((_, index) => ({x: index, y: this.props.currentKey === this.ELO_KEY ? eloYTicks[0] : -1}))
                                .filter(({x}) => x > this.props.matchesShown[0] && x < this.props.matchesShown[1])
                            }
                        />
                        <Line
                            opacity={0}
                            data={[
                                {x: -1, y: this.props.currentKey === this.ELO_KEY ? eloYTicks[eloYTicks.length - 1] : 1},
                                ...(this.props.matches.map((_, index) => ({x: index, y: this.props.currentKey === this.ELO_KEY ? eloYTicks[eloYTicks.length - 1] : 1})))
                                ]
                                .filter(({x}) => x >= this.props.matchesShown[0] - 2 && x < this.props.matchesShown[1])
                            }
                        />
                        <HorizontalGridLines tickValues={this.props.currentKey === this.ELO_KEY ? [950, 1100, 1250] : [this.props.averageKd]}/>
                        {Object.keys(this.props.players).filter((name) => this.props.players[name].show).map((name, index) => (
                            <Line
                                // opacity={this.props.players[name].show ? 1 : 0}
                                color={this.props.players[name].color}
                                key={name + '-line'}
                                data={this.props.players[name][this.props.currentKey]
                                    .filter(({x}) => x >= this.props.matchesShown[0] - 1 && x < this.props.matchesShown[1])}
                                onSeriesMouseOver={this.props.players[name].show ? () => this.setState({hoverName: name}) : undefined}
                                onSeriesMouseOut={() => this.setState({hoverName: undefined})}
                            />
                        ))}
                        <Crosshair
                            values={this.state.crosshairValues}
                            itemsFormat={(points) => {
                                const names = Object.keys(this.props.players).filter((name) => this.props.players[name].show);
                                let delta = 0;
                                let teamDiff;
                                let x;
                                const vals = points.map((point, index) => {
                                    if (!point) {
                                        return null;
                                    }
                                    x = point.x;
                                    const currentEloIndex = this.props.players[names[index]][this.props.currentKey].findIndex(({x}) => x === point.x);
                                    const prevElo = this.props.players[names[index]][this.props.currentKey][currentEloIndex - 1];
                                    delta = (prevElo && this.props.currentKey === this.ELO_KEY ? ` (${point.y > prevElo.y ? '+' : ''}${point.y - prevElo.y})` : '');
                                    return {
                                        value: point.y + delta,
                                        title: names[index]
                                    }
                                });
                                if (x && this.props.matches[x].elos) {
                                    const elos = JSON.parse(this.props.matches[x].elos.S);
                                    teamDiff = elos.friendlyElo - elos.enemyElo;
                                }
                                return [
                                    ...vals,
                                    ...(!teamDiff ? [] : [{
                                        value: teamDiff > 0 ? '+' + teamDiff : teamDiff,
                                        title: 'Team elo diff'
                                    }])
                                ];
                            }}
                            titleFormat={(points) => {
                                const point = points.find((pt, idx) => {
                                    return pt !== undefined;
                                });
                                if (point) {
                                    const match = this.props.matches[point.x];
                                    if (match.map) {
                                        return {
                                            value: match.map.S,
                                            title: 'Map'
                                        };
                                    }
                                }
                            }}
                        />
                        <XAxis
                            tickValues={this.props.matches
                                .map((_, index) => index)
                                .filter((_, x) => x >= this.props.matchesShown[0] - 1 && x < this.props.matchesShown[1])
                            }
                            tickFormat={(value) => this.props.matches[value] ? new Date(parseInt(this.props.matches[value].startTime.N)).toLocaleString() : ''}
                            tickLabelAngle={-90}
                        />
                        <YAxis
                            tickValues={this.props.currentKey === this.ELO_KEY ? eloYTicks : undefined}
                        />
                    </XYPlot>
                </div>
            </div>
        )
    }
    getPageWidth = () => document.getElementById('root').getBoundingClientRect().width;
    getEloYTicks = () => {
        const minEloTick = this.getLowestTick(this.props.minElo);
        const maxEloTick = this.getLargestTick(this.props.maxElo);
        const eloYTicks = [];
        for (let i = minEloTick; i <= maxEloTick; i += this.Y_TICK_SIZE) {
            eloYTicks.push(i);
        }
        return eloYTicks;
    }
    getLowestTick = (lowestValue) => {
        let nearestTick = Math.floor(lowestValue / 100) * 100;
        for (let i = 0; i < (100 / this.Y_TICK_SIZE); i++) {
            if (nearestTick + this.Y_TICK_SIZE < lowestValue) {
                nearestTick += this.Y_TICK_SIZE;
            } else {
                break;
            }
        }
        return nearestTick;
    }

    getLargestTick = (largestValue) => {
        let nearestTick = Math.ceil(largestValue / 100) * 100;
        for (let i = 0; i < (100 / this.Y_TICK_SIZE); i++) {
            if (nearestTick - this.Y_TICK_SIZE > largestValue) {
                nearestTick -= this.Y_TICK_SIZE;
            } else {
                break;
            }
        }
        return nearestTick;
    }
}
