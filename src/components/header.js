import * as React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import SplitButton from 'react-bootstrap/SplitButton';
import Dropdown from 'react-bootstrap/Dropdown';
import Nav from 'react-bootstrap/Nav';

import Auth from '@aws-amplify/auth';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: false
        };
    }
    render() {
        return (
            <Navbar variant="dark" bg="primary" expand="md" expanded={this.state.expanded} onToggle={(expanded) => this.setState({expanded})}>
                <Navbar.Brand>TEXNA FaceIt Tracker</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <div className="mr-auto">{this.props.children}</div>
                    <Nav className="justify-content-end">
                        <SplitButton title={this.props.username} variant="outline-success" alignRight>
                            <Dropdown.Item onClick={() => Auth.signOut()}>Logout</Dropdown.Item>
                        </SplitButton>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
