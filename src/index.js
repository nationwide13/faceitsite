import React from 'react';
import ReactDOM from 'react-dom';
import { Authenticator } from 'aws-amplify-react';

import App from './App';
import AuthComponents from './components/auth';

import * as serviceWorker from './serviceWorker';

import './utilities/amplify';

import "bootstrap-slider/dist/css/bootstrap-slider.css"

import './App.css';
import './index.css';
import './bootstrap.min.css';
import '../node_modules/react-vis/dist/style.css';

class Main extends React.Component {
    constructor() {
        super();
        this.state = {
            authState: 'signUp'
        };
    }

    render() {
        return (
            <div className="App">
                <Authenticator hideDefault={true} onStateChange={(authState) => this.setState({authState})}>
                    <AuthComponents.SignIn/>
                    <AuthComponents.SignUp/>
                    <App/>
                </Authenticator>
            </div>
        );
    }
}

ReactDOM.render(<Main />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
