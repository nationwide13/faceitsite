import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { XYPlot, XAxis, YAxis, HorizontalGridLines, LineMarkSeries as Line, Crosshair } from 'react-vis';

import { GRAPH_COLORS } from './utilities/constants';
import Header from './components/header';
import { API, Analytics } from './utilities/amplify';

export default class extends React.Component {
    listener = undefined;

    eventSubmitted = false;
    PLOT_CLASSNAME = 'rv-xy-plot__inner';
    GRID_LINE_CLASSNAME = 'rv-xy-plot__grid-lines';
    LEFT_SHIFT = -10;
    Y_TICK_SIZE = 25;
    ELO_KEY = 'elo';
    KD_KEY = 'kd';

    constructor(props) {
        super(props);
        this.state = {
            matches: [],
            loadError: undefined,
            players: {},
            hoverName: '',
            crosshairValues: [],
            minElo: 9999999999999,
            maxElo: 0,
            pageWidth: this.getPageWidth(),
            EloYTicks: [],
            loading: true,
            averageKd: 0,
            currentKey: this.ELO_KEY
        };
        this.listener = window.addEventListener('resize', () => this.setState({pageWidth: this.getPageWidth()}))

    }
    getPageWidth = () => document.getElementById('root').getBoundingClientRect().width;
    componentWillUnmount() {
        if (this.listener) {
            window.removeEventListener('resize', this.listener);
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.authData && !this.eventSubmitted) {
            this.eventSubmitted = true;
            Analytics.record({name: 'homepage', attributes: {username: newProps.authData.username}});
        }
    }

    componentDidMount() {
        this.loadMatches();
    }

    render() {
        if (this.props.authState !== 'signedIn' || this.state.loading) {
            return null;
        }
        const username = this.props.authData.username;
        return (
            <div>
                <Header username={username}/>
                <div>
                    <h3>{this.state.currentKey === this.ELO_KEY ? 'Elo' : `K/D (avg: ${this.state.averageKd})`}</h3>
                    <Button>Options</Button>
                    <div>
                        {Object.keys(this.state.players).map((name, index) => (
                            <Form.Check
                                type="checkbox"
                                id={'check-' + name}
                                label={name + ' - ' + (this.state.currentKey === this.ELO_KEY ? this.state.players[name].currentElo : this.state.players[name].averageKd)}
                                checked={this.state.players[name].show}
                                onChange={(event) => {
                                    const players = this.state.players;
                                    players[name].show = event.target.checked;
                                    this.setState({players});
                                }}
                                key={name + '-legend'}
                                style={{
                                    display: 'inline-block',
                                    margin: '0 1rem 1rem',
                                    ...this.state.hoverName === name ? (
                                        {color: '#000', backgroundColor: GRAPH_COLORS[index]}
                                    ) : (
                                        {color: GRAPH_COLORS[index]}
                                    )
                                }}
                            />
                        ))}
                    </div>
                </div>
                <div style={{alignItems: 'center', margin: '0 auto', padding: '0 20px'}}>
                    <div style={{flex: 2}}>
                        <XYPlot
                            width={this.state.pageWidth - 40}
                            height={500}
                            margin={{bottom: 150}}
                            onMouseLeave={() => this.setState({crosshairValues: []})}
                            onMouseMove={(e) => {
                                const targetClassname = this.PLOT_CLASSNAME;
                                let target = e.target;
                                let checkCount = 0;
                                while (target.className.baseVal !== targetClassname || checkCount > 5) {
                                    target = target.parentElement;
                                }
                                target = target.getElementsByClassName(this.GRID_LINE_CLASSNAME)[0];
                                const rect = target.getBoundingClientRect();
                                const cursorX = e.clientX - rect.left;
                                const percent = (cursorX / rect.width);
                                const xCoord = Math.round(Math.round(percent * 100000) / 100000 * this.state.matches.length);
                                let crosshairValues = [];
                                if ((xCoord && isFinite(xCoord)) || xCoord === 0) {
                                    const xVal = xCoord - 1;
                                    crosshairValues = Object.keys(this.state.players).map((name) => this.state.players[name][this.state.currentKey].find(({x}) => x === xVal));
                                }
                                this.setState({crosshairValues});
                            }}
                        >
                            <Line opacity={0} data={this.state.matches.map((_, index) => ({x: index, y: this.state.currentKey === this.ELO_KEY ? this.state.minEloTick : -1}))}/>
                            <Line opacity={0} data={[
                                {x: -1, y: this.state.currentKey === this.ELO_KEY ? this.state.maxEloTick : 5},
                                ...(this.state.matches.map((_, index) => ({x: index, y: this.state.currentKey === this.ELO_KEY ? this.state.maxEloTick : 5})))
                            ]}/>
                            <HorizontalGridLines tickValues={this.state.currentKey === this.ELO_KEY ? [950, 1100] : [this.state.averageKd]}/>
                            {Object.keys(this.state.players).map((name, index) => (
                                <Line
                                    opacity={this.state.players[name].show ? 1 : 0}
                                    color={GRAPH_COLORS[index]}
                                    key={name + '-line'}
                                    data={this.state.players[name][this.state.currentKey]}
                                    onSeriesMouseOver={this.state.players[name].show ? () => this.setState({hoverName: name}) : undefined}
                                    onSeriesMouseOut={() => this.setState({hoverName: undefined})}
                                />
                            ))}
                            <Crosshair
                                values={this.state.crosshairValues}
                                itemsFormat={(points) => {
                                    const names = Object.keys(this.state.players);
                                    let delta = 0;
                                    let teamDiff;
                                    let x;
                                    const vals = points.map((point, index) => {
                                        if (!point) {
                                            return null;
                                        }
                                        x = point.x;
                                        const currentEloIndex = this.state.players[names[index]][this.state.currentKey].findIndex(({x}) => x === point.x);
                                        const prevElo = this.state.players[names[index]][this.state.currentKey][currentEloIndex - 1];
                                        delta = (prevElo && this.state.currentKey === this.ELO_KEY ? `${point.y > prevElo.y ? '+' : ''}${point.y - prevElo.y}` : '');
                                        return {
                                            value: point.y,
                                            title: names[index]
                                        }
                                    });
                                    if (x && this.state.matches[x].elos) {
                                        const elos = JSON.parse(this.state.matches[x].elos.S);
                                        teamDiff = elos.friendlyElo - elos.enemyElo;
                                    }
                                    return [
                                        ...vals,
                                        ...(delta !== 0 ? [{
                                            value: delta,
                                            title: 'Elo gain/loss'
                                        }] : []),
                                        ...(teamDiff ? [{
                                            value: teamDiff > 0 ? '+' + teamDiff : teamDiff,
                                            title: 'Team Elo diff'
                                        }] : []),
                                    ]
                                }}
                                titleFormat={(points) => {
                                    let index = 0;
                                    const point = points.find((pt, idx) => {
                                        index = idx;
                                        return pt !== undefined;
                                    });
                                    if (point) {
                                        const match = this.state.matches[point.x];
                                        if (match.map) {
                                            return {
                                                value: match.map.S,
                                                title: 'Map'
                                            };
                                        }
                                    }
                                }}
                            />
                            <XAxis
                                tickValues={this.state.matches.map((_, index) => index)}
                                tickFormat={(value) => this.state.matches[value] ? new Date(parseInt(this.state.matches[value].startTime.N)).toLocaleString() : ''}
                                tickLabelAngle={-90}
                            />
                            <YAxis
                                tickValues={this.state.currentKey === this.ELO_KEY ? this.state.eloYTicks : undefined}
                            />
                        </XYPlot>
                        <Button onClick={() => this.setState({currentKey: this.state.currentKey === this.ELO_KEY ? this.KD_KEY : this.ELO_KEY})}>Toggle graph</Button>
                    </div>
                </div>
            </div>
        );
    }

    loadMatches = async () => {
        try {
            const result = await API.get('/list?game=battalion&limit=10');
            const players = {};
            const matches = result.Items.reverse();
            let minElo = this.state.minElo;
            let maxElo = this.state.maxElo;
            const kds = [];
            matches.forEach((match, index) => {
                const playerData = JSON.parse(match.playerData.S);
                playerData.forEach((player) => {
                    const name = player.name;
                    const elo = player.afterElo;
                    const kd = player.kd;
                    if (!players[name]) {
                        players[name] = {
                            elo: [],
                            kd: [],
                            show: true
                        };
                    }
                    if (elo) {
                        if (elo > maxElo) {
                            maxElo = elo;
                        }
                        if (elo < minElo) {
                            minElo = elo;
                        }
                        players[name].elo.push({x: index, y: elo});
                        players[name].kd.push({x: index, y: kd});
                        players[name].currentElo = elo;
                        kds.push(parseFloat(kd));
                    }

                });
            });
            const averageKd = Math.round(kds.reduce((total, num) => total += num) / kds.length * 100) / 100;
            Object.keys(players).forEach((player) => {
                let playerKds = players[player].kd.map(({y}) => parseFloat(y)).sort();
                const filteredKds = playerKds.length > 2 ? playerKds.slice(2, players[player].kd.length) : playerKds;
                const filteredSummedKds = filteredKds.reduce((total, num) => total += num);
                const averageSummedKds = playerKds.reduce((total, num) => total += num);
                players[player].filteredKd = Math.round(filteredSummedKds / filteredKds.length * 100) / 100;
                players[player].averageKd = Math.round(averageSummedKds / playerKds.length * 100) / 100;
                // console.log(`${player} - avg ${players[player].averageKd} - filtered ${players[player].filteredKd} - diff ${Math.round((players[player].averageKd - players[player].filteredKd) * 100) / 100}`)
            });
            const minEloTick = this.getLowestTick(minElo);
            const maxEloTick = this.getLargestTick(maxElo);
            const eloYTicks = [];
            for (let i = minEloTick; i <= maxEloTick; i += this.Y_TICK_SIZE) {
                eloYTicks.push(i);
            }
            this.setState({matches, players, eloYTicks, maxEloTick, minEloTick, averageKd, loading: false});
        } catch (loadError) {
            this.setState({loadError});
        }
    }

    getLowestTick = (lowestValue) => {
        let nearestTick = Math.floor(lowestValue / 100) * 100;
        for (let i = 0; i < (100 / this.Y_TICK_SIZE); i++) {
            if (nearestTick + this.Y_TICK_SIZE < lowestValue) {
                nearestTick += this.Y_TICK_SIZE;
            } else {
                break;
            }
        }
        return nearestTick;
    }

    getLargestTick = (largestValue) => {
        let nearestTick = Math.ceil(largestValue / 100) * 100;
        for (let i = 0; i < (100 / this.Y_TICK_SIZE); i++) {
            if (nearestTick - this.Y_TICK_SIZE > largestValue) {
                nearestTick -= this.Y_TICK_SIZE;
            } else {
                break;
            }
        }
        return nearestTick;
    }
}
