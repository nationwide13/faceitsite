import Amplify, { API as AmplifyAPI, Analytics as AmplifyAnalytics } from 'aws-amplify';
import aws_exports from '../aws-exports';

Amplify.configure(aws_exports);
AmplifyAPI.configure({
    API: {
        endpoints: [
            {
                name: "api791107a9",
                endpoint: "https://rkiw1z1hz0.execute-api.us-west-2.amazonaws.com/prod",
                // custom_header: async () => {
                //     // return { Authorization : 'token' }
                //     // Alternatively, with Cognito User Pools use this:
                //     const token = (await Auth.currentSession()).accessToken.jwtToken;
                //     console.log(token);
                //     return { Authorization: `${(token)}` }
                // }
            }
        ]
    }
});

export const Analytics = AmplifyAnalytics;

let apiName = aws_exports.aws_cloud_logic_custom[0].name;

// let myInit = { // OPTIONAL
//     headers: {}, // OPTIONAL
//     response: true, // OPTIONAL (return the entire Axios response object instead of only response.data)
//     queryStringParameters: {  // OPTIONAL
//         name: 'param'
//     }
// };
export const API = {
    get: async (path, config) => AmplifyAPI.get(apiName, path, config),
    post: (path, config) => AmplifyAPI.post(apiName, path, config)
};
